import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { elements } from '../../assets/constants/elements';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController
  ) {
  }
  sendToList() {

    this.navCtrl.push('ListPage');
  }

}
