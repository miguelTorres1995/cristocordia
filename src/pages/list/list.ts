import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { elements } from '../../assets/constants/elements';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})

export class ListPage {
  public list: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = elements;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }

  sendToSlidePage(item){
  this.navCtrl.push('SlidesPage',{
      obj:item,
    }); 
  }

}
