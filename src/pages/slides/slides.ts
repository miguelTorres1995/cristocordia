import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { elements } from '../../assets/constants/elements';

/**
 * Generated class for the SlidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slides',
  templateUrl: 'slides.html',
})
export class SlidesPage {
  currentItem: any;
  public list: any[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController) {
    this.list = elements;

  }


  ionViewDidLoad() {
    this.currentItem = this.navParams.get('obj');
  }

  openDialog(message, option) {
    if (option == 1) {
      let alert = this.alertCtrl.create({
        title: 'Reto ;)',
        subTitle: message,
        buttons: ['Ok']
      });
      alert.present();
    }else if (option == 2) {
      let alert = this.alertCtrl.create({
        title: 'Cita biblica XD',
        subTitle: message,
        buttons: ['Ok']
      });
      alert.present();
    }
  }





}
